const express = require('express')
const router = express.Router()
const courseController = require('../controllers/courseController.js')
const auth = require('../auth.js')

//route for creating a course

router.post("/", auth.verify, (req, res) => {

    const userData = auth.decode(req.headers.authorization)

    courseController.addCourse(req.body, {userId: userData.id, isAdmin:userData.isAdmin}).then(resultFromController => res.send(resultFromController))
})

module.exports = router